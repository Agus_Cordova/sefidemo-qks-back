package org.acoras.repositories;


import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import org.acoras.models.User;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {

    public ArrayList<User> findAllUsers() {
        PanacheQuery<User> all = findAll();
        return (ArrayList)all.list();
    }

    public User findByName(String nombre) {
        return find("nombre", nombre).firstResult();
    }

    public User findByUserId(Long id) {
        return find("id", id).firstResult();
    }

    public boolean save(User user) {
        persist(user);
        return isPersistent(user);
    }

    public boolean doUpdate(User user) {
        update("nombre = :nombre," +
            " primer_apellido = :primer_apellido," +
            " segundo_apellido = :segundo_apellido" +
            " where id = :id",
            Parameters.with("nombre", user.getNombre())
                .and("primer_apellido", user.getPrimerApellido())
                .and("segundo_apellido", user.getSegundoApellido())
                .and("id", user.getId())
        );
        return isPersistent(user);
    }


}
