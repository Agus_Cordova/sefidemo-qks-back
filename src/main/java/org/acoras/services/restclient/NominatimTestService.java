package org.acoras.services.restclient;

import org.acoras.models.NominatimResponse;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("search")
@RegisterRestClient(configKey = "test-api")
public interface NominatimTestService {


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    Set<NominatimResponse> getByName(
        @DefaultValue("xalapa") @QueryParam("q") String params,
        @DefaultValue("json") @QueryParam("format") String format
    );
}


