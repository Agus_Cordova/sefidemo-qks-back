package org.acoras.services.restclient;


import org.acoras.models.gau.GAURequest;
import org.acoras.models.gau.GAUResponse;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@RegisterRestClient
@Path("Users")
public interface GAUService {


    @POST
    @Path("Login")
    @Produces(MediaType.APPLICATION_JSON)
    GAUResponse attemptLogin(GAURequest request);


    @POST
    @Path("Logout")
    @Produces(MediaType.APPLICATION_JSON)
    GAUResponse logout(GAURequest request);

    @POST
    @Path("Password/Change")
    @Produces(MediaType.APPLICATION_JSON)
    GAUResponse changePassword(GAURequest request);

    @POST
    @Path("Password/Reset")
    @Produces(MediaType.APPLICATION_JSON)
    GAUResponse resetPassword(GAURequest request);


}
