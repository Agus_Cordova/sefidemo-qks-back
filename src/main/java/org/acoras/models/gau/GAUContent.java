package org.acoras.models.gau;


import lombok.Data;

@Data
public class GAUContent {
    private GAUToken token;
    private GAUUser usuario;

}
