package org.acoras.models.gau;


import lombok.Data;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

@Data
public class GAUResponse {

    private Boolean env;
    private Boolean success;

    private GAUContent content;

    private String stringContent;
    private String generated;
    private String apiVersion;


    @JsonbCreator
    public GAUResponse(
            @JsonbProperty("env") Boolean env,
            @JsonbProperty("success") Boolean success,
            @JsonbProperty("content") GAUContent content,
            @JsonbProperty("content") String stringContent,
            @JsonbProperty("generated") String generated,
            @JsonbProperty("api_version") String apiVersion) {
        this.env = env;
        this.success = success;
        this.stringContent = stringContent;
        this.content = content;
        this.generated = generated;
        this.apiVersion = apiVersion;
    }
}
