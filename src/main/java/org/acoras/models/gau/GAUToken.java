package org.acoras.models.gau;


import lombok.Data;

import javax.json.bind.annotation.JsonbProperty;

@Data
public class GAUToken {

    @JsonbProperty("TokenType")
    private String tokenType;

    @JsonbProperty("Token")
    private String token;

    @JsonbProperty("ExpiresIn")
    private Integer expiresIn;

}
