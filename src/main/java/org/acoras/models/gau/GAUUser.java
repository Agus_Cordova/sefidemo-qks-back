package org.acoras.models.gau;




import lombok.Data;

import javax.json.bind.annotation.JsonbProperty;
import java.util.List;


@Data
public class GAUUser {

    @JsonbProperty(value = "UsuarioAppId", nillable = true)
    private int usuarioAppId;

    @JsonbProperty(value = "UsuarioId", nillable = true)
    private int usuarioId;

    @JsonbProperty(value = "Usuario", nillable = true)
    private String usuario;

    @JsonbProperty(value = "Nombre", nillable = true)
    private String nombre;

    @JsonbProperty(value = "Paterno", nillable = true)
    private String paterno;

    @JsonbProperty(value = "Materno", nillable = true)
    private String materno;

    @JsonbProperty(value = "FechaExpiraPassword", nillable = true)
    private String fechaExpiraPassword;

    @JsonbProperty("Roles")
    private List<GAURole> roles;

    @JsonbProperty(value = "TipoUsuarioId", nillable = true)
    private int tipoUsuarioId;

    @JsonbProperty(value = "TipoUsuario", nillable = true)
    private String tipoUsuario;

    @JsonbProperty(value = "Rfc", nillable = true)
    private String rfc;

    @JsonbProperty(value = "Curp", nillable = true)
    private String curp;

    @JsonbProperty(value = "CorreoElectronico", nillable = true)
    private String correoElectronico;

    @JsonbProperty(value = "Estatus", nillable = true)
    private int estatus;

    @JsonbProperty(value = "FechaAlta", nillable = true)
    private String fechaAlta;

    @JsonbProperty(value = "FechaCambioPassword", nillable = true)
    private String fechaCambioPassword;

    @JsonbProperty(value = "Cr", nillable = true)
    private String cr;

    @JsonbProperty(value = "CrDescripcion", nillable = true)
    private String crDescripcion;

    @JsonbProperty(value = "Area", nillable = true)
    private String area;


}
