package org.acoras.models.gau;

import lombok.Data;

import javax.json.bind.annotation.JsonbProperty;

@Data
public class GAURole {

    @JsonbProperty("RolId")
    private Integer rolId;
    @JsonbProperty("Rol")
    private String rol;
}
