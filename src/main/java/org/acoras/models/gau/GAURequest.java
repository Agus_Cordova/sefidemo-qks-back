package org.acoras.models.gau;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import org.acoras.jsonviews.Views;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

@Data
public class GAURequest {

    private String usuario;

    private String password;

    private String app;

    private String correo;

    private String token;

    private Boolean allsessions;

    public GAURequest() {
    }

//    @JsonbCreator
//    public GAURequest(@JsonbProperty("usuario") String usuario) {
//        this.usuario = usuario;
//    }

    @JsonbCreator
    public GAURequest(@JsonbProperty("usuario") String usuario,
                      @JsonbProperty("password") String password,
                      @JsonbProperty("app") String app) {
        this.usuario = usuario;
        this.password = password;
        this.app = app;
    }
}
