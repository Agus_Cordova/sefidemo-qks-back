package org.acoras.models;

import lombok.Data;

import javax.json.bind.annotation.JsonbProperty;

@Data
public class NominatimResponse {

    @JsonbProperty("place_id")
    private Integer placeId;
    private String lat;
    private String lon;
    @JsonbProperty("display_name")
    private String displayName;

    public NominatimResponse() {
    }

    public NominatimResponse(Integer placeId, String lat, String lon, String displayName) {
        this.placeId = placeId;
        this.lat = lat;
        this.lon = lon;
        this.displayName = displayName;
    }
}
