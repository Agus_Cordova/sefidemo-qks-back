package org.acoras.resources;

import com.fasterxml.jackson.annotation.JsonView;
import org.acoras.jsonviews.Views;
import org.acoras.models.gau.GAURequest;
import org.acoras.models.gau.GAUResponse;
import org.acoras.services.restclient.GAUService;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@RequestScoped
@Path("gau-auth")
@Produces(MediaType.APPLICATION_JSON)
public class GAUResource {

    public static final Logger LOGGER = LoggerFactory.getLogger(GAUResource.class);

    @Inject
    @RestClient
    private GAUService gauService;


    @POST
    @Path("login")
    public GAUResponse signIn(GAURequest request) {
        LOGGER.debug("++++ GAU LOGIN Request: " + request);
        GAUResponse gauResponse = gauService.attemptLogin(request);
        LOGGER.debug("++++ GAU LOGIN RESPONSE: " + gauResponse);
        return  gauResponse;

    }

    @POST
    @Path("logout")
    public GAUResponse signOut(GAURequest request) {
        LOGGER.debug("++++ GAU LOGOUT Request: " + request);
        GAUResponse gauResponse = gauService.logout(request);
        LOGGER.debug("++++ GAU LOGOUT RESPONSE: " + gauResponse);
        return  gauResponse;

    }


    @POST
    @Path("change-pass")
    public GAUResponse changePassword(GAURequest request) {
        LOGGER.debug("++++ GAU CHANGE PASS REQUEST: " + request);
        GAUResponse gauResponse = gauService.changePassword(request);
        LOGGER.debug("++++ GAU CHANGE PASS RESPONSE: " + gauResponse);
        return  gauResponse;

    }

    @POST
    @Path("reset-pass")
    public GAUResponse resetPassword(GAURequest request) {
        LOGGER.debug("++++ GAU RESET PASS REQUEST: " + request);
        GAUResponse gauResponse = gauService.resetPassword(request);
        LOGGER.debug("++++ GAU RESET PASS RESPONSE: " + gauResponse);
        return  gauResponse;

    }

}
