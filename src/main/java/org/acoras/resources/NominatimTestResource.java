package org.acoras.resources;

import org.acoras.models.NominatimResponse;
import org.acoras.services.restclient.NominatimTestService;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;


@Path("location")
public class NominatimTestResource {

    public static final Logger LOGGER = LoggerFactory.getLogger(NominatimTestResource.class);

    @Inject
    @RestClient
    NominatimTestService nominatimTestService;


    @GET
    @Path("{place}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<NominatimResponse> getNominatimResults(@PathParam String place) {
        LOGGER.debug("+++++ PARAMS: " + place);
        return nominatimTestService.getByName(place, "json");

    }

}
