package org.acoras.resources;

import org.acoras.models.User;
import org.acoras.repositories.UserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    @Inject
    private UserRepository userRepository;


    @GET
    @Path("/list")
//    public ArrayList<User> getAllUsers() {
    public Response getAllUsers() {
        return Response.ok(userRepository.findAllUsers()).build();
//        return userRepository.findAllUsers();
    }


    @POST
    @Transactional
    public User saveUser(User user) {
        if (userRepository.save(user)) {
            return user;
        } else {
            return null;
        }
    }


    @PUT
    @Transactional
    public User updateUser(User user) {
//        userRepository.persist
        if (userRepository.doUpdate(user)) {
            return user;
        } else {
            return null;
        }
    }

    @DELETE
    @Transactional
    @Path("/{id}")
    public Response dropUser(@PathParam("id") Long id) {

        User user = userRepository.findByUserId(id);
        if (user != null) {
            userRepository.delete(user);
            return Response.status(Response.Status.ACCEPTED).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }



}
