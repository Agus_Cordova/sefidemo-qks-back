package org.acoras.resources;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;


@QuarkusTest
class NominatimTestResourceTest {

    public static final Logger LOGGER = LoggerFactory.getLogger(NominatimTestResourceTest.class);


    @Test
    void testGetNominatimResults() {
        given().pathParam("thing", "xalapa")
            .when().get("/location/{thing}")
            .then()
                .log().headers()
                .statusCode(200)
                .body(
//                        "$.size()", is(10),
                        containsString("Xalapa, Veracruz de Ignacio de la Llave, México")
//                "[0].lat", is("19.54203315"),
//                    "[0].lon", is("-96.89982687205952"),
//                    "[0].display_name", is("Xalapa, Veracruz de Ignacio de la Llave, México")
                )

        ;

    }
}