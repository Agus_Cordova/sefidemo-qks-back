package org.acoras.resources;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.acoras.models.gau.GAURequest;
import org.junit.jupiter.api.Test;



import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;


@QuarkusTest
class GAUResourceTest {



    @Test
    void testLoginEndpointWithRestClient() {
        GAURequest req = new GAURequest("test", "Wzfzp0hrjcd$", "test");

        given()
                .contentType(ContentType.JSON)
                .body(req)
            .when().post("/gau-auth/login")
            .then()
            .body(
                "success", is(true),
                "content.usuario.Roles[0].Rol", is("administrador")
            );

    }

    @Test
    void testWrongLoginEndpointWithRestClient() {
        GAURequest req = new GAURequest("test", "incorrect_pass", "test");

        given()
                .contentType(ContentType.JSON)
                .body(req)
            .when().post("/gau-auth/login")
            .then()
            .body(
                "success", is(false),
                "content.usuario.Roles[0].Rol", is( not("administrador"))
            );

    }
    @Test
    void testLogOut() {
        GAURequest req = new GAURequest();
        req.setAllsessions(false);
        req.setToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InJxdWludGVybyIsIm5iZiI6MTU4ODAxMjAwNywiZXhwIjoxNjE5NTQ4MDA3LCJpYXQiOjE1ODgwMTIwMDcsImlzcyI6Imh0dHA6Ly9nYXUuc3NhdmVyLmdvYi5teCIsImF1ZCI6Imh0dHA6Ly9nYXUuc3NhdmVyLmdvYi5teCJ9.zfvKjmXzuCCzSsj2xTGkvTIX5rlJNXXxn3pG0BkTnTc");

        given()
                .contentType(ContentType.JSON)
                .body(req)
            .when().post("/gau-auth/logout")
            .then()
            .body(
                "success", is(true),
                "content", is( not("Se cerro session correctamente."))
            );

    }

    @Test
    void testChangePass() {
        GAURequest req = new GAURequest();
        req.setUsuario("test");
        given()
                .contentType(ContentType.JSON)
                .body(req)
            .when().post("/gau-auth/change-pass")
            .then()
            .body(
                "success", is(true),
                "content", is( not("Se envio el correo correctamente"))
            );

    }

    @Test
    void testResetPass() {
        GAURequest req = new GAURequest();
        req.setUsuario("test");
        given()
                .contentType(ContentType.JSON)
                .body(req)
            .when().post("/gau-auth/reset-pass")
            .then()
            .body(
                "success", is(true),
                "content", is( not("Se envio el correo correctamente"))
            );

    }


}